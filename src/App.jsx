import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom'
import Top from './pages/top';
import Question from './pages/question';
import Result from './pages/result';

const App = () => {
  return (
    <BrowserRouter>
      <Route exact path="/" component={Top} />
      <Route exact path="/question" component={Question} />
      <Route exact path="/result/:id" component={Result} />
    </BrowserRouter>
  );
};

export default App;
