import React from 'react';
import { withRouter } from 'react-router';
import { withStyles, FormLabel, RadioGroup, Radio, FormControlLabel, Fab } from '@material-ui/core';
import tapinatorLogo from '../img/tapinator-logo.svg';

class Question extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: null,
    };
  }

  handleChange = (e) => {
    this.setState({ value: e.target.value });
  };

  componentDidMount() {
    fetch("https://tapickathon-api.herokuapp.com/questions", { mode: 'cors' })
      .then(response => response.json())
      .then(json => {
        this.setState({ questions: json });
        console.log(json);
      })
      .catch(error => console.log(error));
  }

  handleClick = () => {
    const body = new FormData(document.forms.questions);
    fetch("https://tapickathon-api.herokuapp.com/result", { mode: 'cors', method: 'POST', body: body })
      .then(response => response.json())
      .then(json => this.props.history.push({ pathname: `/result/${json.id}` }))
      .catch(error => console.log(error));
  }

  render() {
    const { classes } = this.props;
    const { value, questions } = this.state;
    return (
      <React.Fragment>
        <img src={tapinatorLogo} alt="logo" className={classes.block} />

        <form id="questions" className={classes.root}>
          {questions && questions.map((question) => {
            return (
              <React.Fragment>
                <FormLabel>{`Q.${question.title}`}</FormLabel>
                <RadioGroup
                  name={question.id}
                  className={classes.group}
                  value={value}
                  onChange={this.handleChange}
                >
                  <FormControlLabel value="1" control={<Radio />} label={question.Q1} />
                  <FormControlLabel value="2" control={<Radio />} label={question.Q2} />
                </RadioGroup>
              </React.Fragment>
            );
          })}
          <Fab
            variant="extended"
            color="secondary"
            size="large"
            onClick={this.handleClick}
          >診断結果を見る！</Fab>
        </form>
      </React.Fragment>
    );
  }
}

const styles = {
  'root': {
    margin: '25px'
  },
  'block': {
    display: 'block',
    margin: '0px 25px 25px 25px',
  },
  'group': {
    margin: '10px',
  }
};

export default withRouter(withStyles(styles)(Question));