import React from 'react';
import { Link } from 'react-router-dom';
import { Fab, withStyles } from '@material-ui/core';
import tapinatorLogo from '../img/tapinator-logo.svg';
import picture from '../img/picture.svg';

const top = (props) => {
  const { classes } = props;
  return (
    <React.Fragment>
      <img src={tapinatorLogo} alt="logo" className={classes.block} />
      <img src={picture} alt="logo" className={classes.block} />
      <Link to="/question">
        <Fab
          variant="extended"
          color="secondary"
          size="large"
        >診断を始める！</Fab>
      </Link>
    </React.Fragment>
  );
};

const styles = {
  'block': {
    display: 'block',
    margin: '0 25px 25px 25px',
  }
};

export default withStyles(styles)(top);