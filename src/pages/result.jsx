import React from 'react';
import { withStyles } from '@material-ui/core';
import { Link } from 'react-router-dom';
import tapinatorLogo from '../img/tapinator-logo.svg';

class Result extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
    };
  }

  componentDidMount() {
    const { params } = this.props.match;
    fetch(`https://tapickathon-api.herokuapp.com/drinks/${params.id}`)
      .then(response => response.json())
      .then(json => this.setState({ data: json }))
      .catch(error => console.log(error));
  }

  render() {
    const { data } = this.state;
    const { classes } = this.props;
    return (
      <React.Fragment>
        <img src={tapinatorLogo} alt="logo" className={classes.block} />
        {data &&
          <React.Fragment>
            <p>あなたは。。。</p>
            <p className={classes.strong}><span className={classes.span}>{data.name}です！</span></p>
            <img src={data.img_url} alt="logo" className={classes.tapi} />
          </React.Fragment>
        }
        <Link to="/" className={classes.block}>
          もう一度診断する
        </Link>
      </React.Fragment>
    );
  }
}

const styles = {
  'block': {
    display: 'block',
  },
  'tapi': {
    display: 'block',
    height: '80vh',
  },
  'strong': {
    fontWeight: 'bold',
  },
  'span': {
    borderBottom: 'solid 2px #FFF796',
  }
};

export default withStyles(styles)(Result);